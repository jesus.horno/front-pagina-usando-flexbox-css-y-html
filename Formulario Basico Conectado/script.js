//devuelve todas las personas
// fetch("http://localhost:8080/asignacion", {
//     method: 'GET',
//     mode: 'no-cors'
// }).then(response => response.json).then(data=>{console.log(data)})

function CargarPrimerDato(){
fetch("http://localhost:8080/asignacion",{method:'GET'})
    .then((response)=>response.json()
    .then(data=>{ 
        if(data.length>0){
            const primerUsuario=data[0];
            console.log(primerUsuario.usuario);
            document.getElementById('usuario').value = primerUsuario.usuario;
            document.getElementById('apellido').value = primerUsuario.surname;
            document.getElementById('contraseña').value = primerUsuario.password;
            document.getElementById('emailCompañia').value = primerUsuario.company_email;
            document.getElementById('emailPersonal').value = primerUsuario.personal_email;
            document.getElementById('ciudad').value = primerUsuario.city;
            document.getElementById('url').value = primerUsuario.imagen_url;
            document.getElementById('fechaCreacion').value =primerUsuario.created_date;
            document.getElementById('activado').value = primerUsuario.active;
            document.getElementById('fechaFinalizacion').value = primerUsuario.termination_date;
            document.getElementById('id').value = primerUsuario.id_persona;
        }
    }))
}

function EnviarForm(evento){

    evento.preventDefault();//para que no se te vatya la pagina a la mierda
    const data={
        usuario:document.getElementById('usuario').value,
        surname:document.getElementById('apellido').value,
        password:document.getElementById('contraseña').value,
        company_email:document.getElementById('emailCompañia').value,
        personal_email:document.getElementById('emailPersonal').value,
        city:document.getElementById('ciudad').value,
        imagen_url:document.getElementById('url').value,
        created_date:document.getElementById('fechaCreacion').value,
        termination_date:document.getElementById('fechaFinalizacion').value,
        name:document.getElementById('usuario').value,
        imagen_url:document.getElementById('url').value,
        active:document.getElementById('activado').value
    }

    console.log(data);
    console.log(document.getElementById('activado').value);

    fetch("http://localhost:8080/asignacion/"+document.getElementById('id').value , {method:'PUT', headers:{ 'Content-Type':'application/json'},body:JSON.stringify(data)})
}

function limpiarFormulario(){
    let formulario = document.getElementById('contenedorFormulario');
    const lenFormulario = formulario.elements.length;
    for(i=0;i<lenFormulario;i++){
        let elemento = formulario.elements[i];
        if(elemento.type === 'text' || elemento.type === 'password' || elemento.type === 'email' || elemento.type === 'date')
            elemento.value="";
        else if(elemento.type === 'checkbox')
            elemento.checked=false;
    }
}


function formatDate(dateString)
    {
        console.log(dateString);
        var allDate = dateString.split(' ');
        var thisDate = allDate[0].split('-');
        var thisTime = allDate[1].split('T');
        var newDate = [thisDate[2],thisDate[1],thisDate[0] ].join("-");

        var suffix = thisTime[0] >= 12 ? "PM":"AM";
        var hour = thisTime[0] > 12 ? thisTime[0] - 12 : thisTime[0];
        var hour =hour < 10 ? "0" + hour : hour;
        var min = thisTime[1] ;
        var sec = thisTime[2] ;
        var newTime = hour + ':' + min + suffix;

        return newDate + ' ' + newTime;
    }


window.onload = function(){ 
    CargarPrimerDato();
    // document.getElementById('limpiar').onclick = limpiarFormulario;
    document.getElementById('editar').onclick = EnviarForm;
}