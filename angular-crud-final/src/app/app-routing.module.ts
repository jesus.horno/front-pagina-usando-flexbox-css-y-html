import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpdatePersonaComponent } from './actualizar/update-persona/update-persona.component';
import { AddPersonaComponent } from './add/add-persona/add-persona.component';
import { PersonasComponent } from './pages/personas/personas.component';

const routes: Routes = [
  { path: 'actualizar/:id_persona', component: UpdatePersonaComponent},
  { path: 'añadir', component: AddPersonaComponent},
  { path: '', component: PersonasComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
