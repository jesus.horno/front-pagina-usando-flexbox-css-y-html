import { Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { personaINPUT, Response } from 'src/app/modelos';
import { VentanaModalService } from 'src/app/services/ventana-modal.service';

@Component({
  selector: 'app-detail-persona',
  templateUrl: './detail-persona.component.html',
  styleUrls: ['./detail-persona.component.scss']
})
export class DetailPersonaComponent implements OnInit {

  personaDetalle = new MatTableDataSource < personaINPUT > ();

  displayedColumns: string[] = ['usuario',
                                'password',
                                'name',
                                'surname',
                                'company_email',
                                'personal_email',
                                'city',
                                'active',
                                'created_date', 
                                'imagen_url',
                                'termination_date'];

                            
  constructor(private ventanaModalService: VentanaModalService) { }

  ngOnInit(): void {
    this.personaDetalle = new MatTableDataSource(this.ventanaModalService.InformacionDetalle())
    console.log("estamos en el componente de detalle" + this.personaDetalle)
  }

}
