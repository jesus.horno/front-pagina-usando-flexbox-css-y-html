import {Component,OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {personaINPUT} from 'src/app/modelos';
import {PersonasService} from 'src/app/services/personas.service';
import { VentanaModalService } from 'src/app/services/ventana-modal.service';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./persona.scss']
})
export class PersonasComponent implements OnInit {
  displayedColumns: string[] = ['usuario','name','email','ciudad','botones'];

  personas = new MatTableDataSource < personaINPUT > ();

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.personas.filter = filterValue.trim().toLowerCase();
  }

  constructor(private personaService: PersonasService,private ventanaModalService: VentanaModalService,  private dialog: MatDialog ) {}

  ngOnInit(): void {
    console.log("pasamos por el persona")
    this.personaService.CargarUsuarios().subscribe((usuarios: personaINPUT[]) => {
        this.personas = new MatTableDataSource(usuarios)
        console.log("despues de la peticion")
      }
    );
    
  }

  delete(id: number){
    console.log("estamos en el borrado")
    this.personaService.BorrarUsuarios(id).subscribe((usuarios: String) =>{
      // this.personas.data.reduce;
       this.ngOnInit();
    })
  }

  detalle(persona : personaINPUT) {
    console.log("entramos en detalle" +persona.id_persona)
    this.ventanaModalService.CargarDetallePersona(persona, this.dialog);
  }
}
