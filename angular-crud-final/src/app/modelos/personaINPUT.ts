import {
    Response
  } from "./respuesta-intefaz";

export class personaINPUT{
    static personaJson(obj: Response) {
        return new personaINPUT(
          obj['usuario'],
          obj['password'],
          obj['name'],
          obj['surname'],
          obj['company_email'],
          obj['personal_email'],
          obj['city'],
          obj['active'],
          obj['created_date'],
          obj['termination_date'],
          obj['imagen_url'],
          obj['idPersona'],
        );
      }
    
      constructor(
        public usuario: string,
        public password: string,
        public name: string,
        public surname: string,
        public company_email: string,
        public personal_email: string,
        public city: string,
        public active: string,
        public created_date: Date,
        public termination_date: Date,
        public imagen_url: string,
        public id_persona: number
      ) {}
}