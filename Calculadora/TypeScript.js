function numero(aux) {
    if (aux == "1" || aux == "2" || aux == "3" || aux == "4" || aux == "5" || aux == "6" || aux == "7" || aux == "8" || aux == "9" || aux == "0" || aux == ".") {
        return true;
    }
    else {
        return false;
    }
}
function PulsadoBoton(valor1) {
    document.getElementById(valor1).onclick = function () {
        console.log("pulsado el boton");
        console.log(valor1);
        if (!numero(document.getElementById(valor1).value)) {
            PuedeRealizarOperacion();
            document.getElementById('result').value += document.getElementById(valor1).value;
        }
        else {
            document.getElementById('result').value += document.getElementById(valor1).value;
        }
    };
}
function PuedeRealizarOperacion() {
    var numero1 = "";
    var numero2 = "";
    var operacionRea = "";
    var operacion = false;
    document.getElementById('result').value.split("").forEach(function (i) {
        console.log(i);
        if (numero(i) && !operacion) {
            numero1 += i;
        }
        if (numero1.length < 1 && !numero(i)) {
            numero1 += i;
        }
        else {
            if (!numero(i)) {
                operacion = true;
                operacionRea = i;
            }
        }
        if (numero(i) && operacion) {
            numero2 += i;
        }
        console.log(numero1 + " numero 2 " + numero2 + " operacion " + operacionRea);
    });
    if (numero1.length > 0 && numero2.length > 0 && operacionRea.length > 0) {
        OperacionRealizar(Number(numero1), Number(numero2), operacionRea);
    }
}
function Solucion(valor1) {
    document.getElementById(valor1).onclick = function () {
        var numero1 = "";
        var numero2 = "";
        var operacionRea = "";
        var operacion = false;
        document.getElementById('result').value.split("").forEach(function (i) {
            console.log(i);
            if (numero(i) && !operacion) {
                numero1 += i;
            }
            if (numero1.length < 1 && !numero(i)) {
                numero1 += i;
            }
            else {
                if (!numero(i)) {
                    operacion = true;
                    operacionRea = i;
                }
            }
            if (numero(i) && operacion) {
                numero2 += i;
            }
        });
        console.log("nuemro " + numero1);
        OperacionRealizar(Number(numero1), Number(numero2), operacionRea);
    };
}
function Reset(valor1) {
    document.getElementById(valor1).onclick = function () {
        document.getElementById('result').value = "";
        document.getElementById('result').placeholder = "0";
    };
}
window.onload = function () {
    PulsadoBoton("1");
    PulsadoBoton("2");
    PulsadoBoton("3");
    PulsadoBoton("4");
    PulsadoBoton("5");
    PulsadoBoton("6");
    PulsadoBoton("7");
    PulsadoBoton("8");
    PulsadoBoton("9");
    PulsadoBoton("0");
    PulsadoBoton("exp");
    PulsadoBoton("dividir");
    PulsadoBoton("X");
    PulsadoBoton("restar");
    PulsadoBoton("sum");
    PulsadoBoton("coma");
    Solucion("igual");
    Reset("C");
};
function OperacionRealizar(numero1, numero2, operacion) {
    console.log(operacion);
    switch (operacion) {
        case '^': {
            console.log('Se hace el exponente');
            document.getElementById('result').value = Math.pow(numero1, numero2).toString();
            break;
        }
        case '/': {
            console.log('Se divide');
            if (numero2 != 0)
                document.getElementById('result').value = (numero1 / numero2).toString();
            break;
        }
        case 'X': {
            console.log('Se multiplica');
            document.getElementById('result').value = (numero1 * numero2).toString();
            break;
        }
        case '-': {
            console.log('Se resta');
            document.getElementById('result').value = (numero1 - numero2).toString();
            break;
        }
        case '+': {
            console.log('Se suma');
            console.log(numero1);
            console.log("solucion:" + (numero1 + numero2).toString());
            document.getElementById('result').value = (numero1 + numero2).toString();
            break;
        }
    }
}
