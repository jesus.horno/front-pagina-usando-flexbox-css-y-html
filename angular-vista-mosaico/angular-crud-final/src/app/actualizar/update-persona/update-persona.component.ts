import { Component, OnInit } from '@angular/core';
import { personaINPUT, Response } from 'src/app/modelos';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; 
import { PersonasService } from 'src/app/services/personas.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-update-persona',
  templateUrl: './update-persona.component.html',
  styleUrls: ['./update-persona.component.scss']
})
export class UpdatePersonaComponent implements OnInit {
   
  idpersonaUpdate =-1;

formPersonas = new FormGroup({
  usuario: new FormControl("",Validators.required),
      password: new FormControl("",Validators.required),
      name: new FormControl("",Validators.required),
      surname: new FormControl(""),
      company_email: new FormControl("",Validators.email),
      personal_email: new FormControl("",Validators.email),
      city: new FormControl("",Validators.required),
      active: new FormControl(""),
      created_date: new FormControl("",Validators.required),
      termination_date: new FormControl(""),
      imagen_url: new FormControl("")

})

  constructor(private route:ActivatedRoute, private personaServicio:PersonasService, private formBuilder: FormBuilder, private routeback:Router) {
    // formPersonas = this.formulario
    this.route.params.subscribe(informacion => console.log(informacion['id_persona']))
    
    this.formPersonas = new FormGroup({
      usuario: new FormControl("",Validators.required),
      password: new FormControl("",Validators.required),
      name: new FormControl("",Validators.required),
      surname: new FormControl(""),
      company_email: new FormControl("",Validators.email),
      personal_email: new FormControl("",Validators.email),
      city: new FormControl("",Validators.required),
      active: new FormControl(""),
      created_date: new FormControl("",Validators.required),
      termination_date: new FormControl(""),
      imagen_url: new FormControl("")
    
    })
   }

  ngOnInit(): void {
    this.route.params.subscribe(informacion => {
      this.idpersonaUpdate=informacion['id_persona']
      this.personaServicio.CargarUsuario(informacion['id_persona']).subscribe(respuestaPersona => {
        let persona = personaINPUT.personaJson(respuestaPersona)
        console.log(persona.name)
        this.formPersonas = new FormGroup({
          usuario: new FormControl(persona.usuario,Validators.required),
          password: new FormControl(persona.password,Validators.required),
          surname: new FormControl(persona.surname),
          name: new FormControl(persona.name,Validators.required),
          company_email: new FormControl(persona.company_email,Validators.email),
          personal_email: new FormControl(persona.personal_email,Validators.email),
          city: new FormControl(persona.city,Validators.required),
          active: new FormControl(persona.active),
          created_date: new FormControl(persona.created_date,Validators.required),
          termination_date: new FormControl(persona.termination_date),
          imagen_url: new FormControl(persona.imagen_url)
        })        
      });
    })
  }

  editar(){
    if(this.formPersonas.valid){
      this.personaServicio.EditarUsuario(this.idpersonaUpdate,this.formPersonas.value).subscribe(respuesta => {
        console.log(respuesta)
        this.routeback.navigate([''])
      });
      
      
    }else{
      alert("FALTAN CAPOS POR RELLENAR")
    }
  }

  limpiar(){
    this.formPersonas.reset();
  }

  

}
