import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {personaINPUT, Response} from '../modelos';
import{map} from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  // public datos = new personaINPUT();

  constructor(private http:HttpClient) {
    // this.datos = [];
  }

  CargarUsuarios(){
   const url = 'http://localhost:8080/asignacion';
    return this.http.get<personaINPUT[]>(url).pipe(
      map(resp =>{
        return resp;
      })
    );
  }

  CargarUsuario(id:number){
    const url = 'http://localhost:8080/asignacion/'+id;
    return this.http.get<Response>(url);
   }

  BorrarUsuarios(id :number){
    const url = 'http://localhost:8080/asignacion/'+id;
    console.log("esta es la url de borrar: " + url)
    const headers = new HttpHeaders({'content-type' : 'text/plain'
  });
        const options = {
            headers: headers
            };
     return this.http.delete<String>(url, options)
   }

   EditarUsuario(id:number, persona: personaINPUT){
    const url = 'http://localhost:8080/asignacion/'+id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    const json = JSON.stringify(persona)
    console.log("persona " + persona)
    console.log("json " +json)
    return this.http.put<personaINPUT>(url, json, httpOptions)
   }

   AnadirUsuario(persona: personaINPUT){
    const url = 'http://localhost:8080/asignacion';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    const json = JSON.stringify(persona)
    console.log("persona " + persona)
    console.log("json " +json)
    return this.http.post<personaINPUT>(url, json, httpOptions)
   }

}
