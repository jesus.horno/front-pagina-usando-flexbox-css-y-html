import { Injectable } from '@angular/core';
import {personaINPUT, Response} from '../modelos';
import{map} from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { DetailPersonaComponent } from '../ventana-modal/detail-persona/detail-persona.component';

@Injectable({
  providedIn: 'root'
})
export class VentanaModalService {

  informacion:personaINPUT[]

  constructor(private http:HttpClient) { 
    this.informacion=[]
  }

  CargarDetallePersona(persona:personaINPUT, dialog: MatDialog){
    console.log("estamos en detalle persona")
    this.informacion[0] = persona
    dialog.open(DetailPersonaComponent)
   }

   InformacionDetalle(){
      console.log("InformacionDetalle" + this.informacion)
      return this.informacion;
   }
}
