import { Component, OnInit } from '@angular/core';
import { personaINPUT, Response } from 'src/app/modelos';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; 
import { PersonasService } from 'src/app/services/personas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-persona',
  templateUrl: './add-persona.component.html',
  styleUrls: ['./add-persona.component.scss']
})
export class AddPersonaComponent implements OnInit {

  formPersonas = new FormGroup({
    usuario: new FormControl(""),
    password: new FormControl(""),
    name: new FormControl(""),
    surname: new FormControl(""),
    company_email: new FormControl(""),
    personal_email: new FormControl(""),
    city: new FormControl(""),
    active: new FormControl(""),
    created_date: new FormControl(""),
    termination_date: new FormControl(""),
    imagen_url: new FormControl("")
  
  })

  constructor(private route:ActivatedRoute, private personaServicio:PersonasService, private formBuilder: FormBuilder, private routeback:Router) { 
    
  }

  ngOnInit(): void {
  }

  Anadir(){
    this.personaServicio.AnadirUsuario(this.formPersonas.value).subscribe(respuesta => {
      console.log(respuesta)
    });
    this.routeback.navigate([''])
  }

  limpiar(){
    this.formPersonas = new FormGroup({
      usuario: new FormControl(""),
      password: new FormControl(""),
      surname: new FormControl(""),
      name: new FormControl(""),
      company_email: new FormControl(""),
      personal_email: new FormControl(""),
      city: new FormControl(""),
      active: new FormControl(""),
      created_date: new FormControl(""),
      termination_date: new FormControl(""),
      imagen_url: new FormControl("")
    
    })
  }

}
