export interface Response {
    usuario:          string;
    password:         string;
    name:             string;
    surname:          string;
    company_email:    string;
    personal_email:   string;
    city:             string;
    active:           string;
    created_date:     Date;
    termination_date: Date;
    imagen_url:       string;
    idPersona:        number;
}