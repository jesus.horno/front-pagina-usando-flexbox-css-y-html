import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { Hero } from './heroes/hero';
import { MessageService } from './messages.service';



import { HEROES } from './mock-heroes';


@Injectable({
  providedIn: 'root',
})
export class HeroService {

  constructor(private messageService: MessageService) { }

  getHeroes(): Observable<Hero[]> {
    const heroes = of(HEROES);
    this.messageService.add('HeroService: fetched heroes');
    return heroes;
  }
}