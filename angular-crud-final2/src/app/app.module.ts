import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PersonasComponent } from './pages/personas/personas.component';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { DetailPersonaComponent } from './ventana-modal/detail-persona/detail-persona.component';
import { MatDialogModule } from '@angular/material/dialog';
import { UpdatePersonaComponent } from './actualizar/update-persona/update-persona.component';
import { AddPersonaComponent } from './add/add-persona/add-persona.component';



@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    DetailPersonaComponent,
    UpdatePersonaComponent,
    AddPersonaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatDialogModule, 
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
